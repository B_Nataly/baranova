import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class FileCopier implements Runnable {
    ArrayList<File> sources =null;
    ArrayList<File> dest =null;
    FileCopier(String source, String partofFileName, int counter) throws NullPointerException {

        try {
            this.sources = this.getAllFilesContainsString(partofFileName, source); //стринги как пути к файлам
            if(this.sources.size() ==0 )
            {
                throw new IOException();
            }
            this.dest = this.createNewDirectoryForCopy(this.sources, counter);
            if(this.dest.size()==0)
            {
                throw new IOException();
            }
        } catch (IOException ex) {
            System.out.println("The files by path '" + source + "' didn't exist, please check the path");
            throw new NullPointerException();
        }
    }

    public void run()
    {
        long startTime = System.currentTimeMillis();
        long timeSpent = 0;
        Date moment = new Date(startTime);
        SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss");
        System.out.println("The new thread started at: " + ft.format(moment));
        try {
            this.copyFile(); //запуск копирования
        } catch (IOException e) {
            timeSpent = System.currentTimeMillis() - startTime;
        }
        timeSpent = System.currentTimeMillis() - startTime;
        System.out.println("Copy below up to " + timeSpent + " milliseconds");
    }

    private void copyFile() throws IOException{
        for (int i=0; i<this.sources.size();i++){
            InputStream is = null;
            OutputStream os = null;
            try {
                is = new FileInputStream(sources.get(i));
                os = new FileOutputStream(dest.get(i));
                byte[] buffer = new byte[1024];
                int length;
                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
                System.out.println("File has been copied successfully to " + dest.get(i).getPath());
            } catch (IOException ex) {
                System.out.println("Cannot move file from " + sources.get(i).getPath() + " to " + dest.get(i).getPath());
            } catch (RuntimeException ex) {
                System.out.println("Something get wrong " + ex.getMessage());
            } finally {
                is.close();
                os.close();
            }
        }}

    private ArrayList<File> createNewDirectoryForCopy(ArrayList<File> files, int counter) {
        ArrayList<File> destinationPathes = new ArrayList<File>();
        for (int i = 0; i < files.size(); i++) {
            String newDirName = files.get(i).getParent() + "\\NewFolder" + counter + "\\";

            File newDir = new File(newDirName);
            newDir.mkdir();
            File newFile = new File(newDirName + files.get(i).getName());
            destinationPathes.add(newFile);
        }
        return  destinationPathes;
    }

    private  ArrayList<File> getAllFilesContainsString(String partOfFlileName, String folderPath) throws IOException
    {
        File dir = new File(folderPath);
        if (!dir.exists())
        {throw  new IOException(folderPath + " is not exist");}
        ArrayList<File> filesWhichContainsPattern = new ArrayList<File>();
        File[] arrayOfFiles= dir.listFiles();
        for (int i=0; i< arrayOfFiles.length;i++)
        {
            if(arrayOfFiles[i].isFile() && arrayOfFiles[i].getName().contains(partOfFlileName))
            {
                filesWhichContainsPattern.add(arrayOfFiles[i]);
            }
        }
        return filesWhichContainsPattern;
    }
}