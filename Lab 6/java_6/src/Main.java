import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    private static String parentFolder = "D:\\JavaTest";

    public static void main(String[] args) {
        int counter = 1;
        while (true) {
            startCopy(counter);
            counter++;
        }
    }

    private static String getTypedText() {
        System.out.println("Type part of the file name or full file name to copy:");
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    private static void startCopy(int counter) {
        try {
            FileCopier copier = new FileCopier(parentFolder, getTypedText(), counter);
            Thread myTread = new Thread(copier);
            myTread.start();
        } catch (NullPointerException ex) {
            System.out.println("Try choose another folder");
        }
    }
}