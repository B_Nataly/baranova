public class Main {
    public static void main (String[] args)
    {
        String pathToSerialization = "sportcar.dat";
        SportCarSerializator serializator = new SportCarSerializator(new SportCar("red", "Porche", "GTX911", 250, 3, false));
        String path = serializator.serializeCar(pathToSerialization);
        System.out.println("Serialized car was saved in file "+ path);
        serializator.deserialize(pathToSerialization).GetFullInfoAboutThisCar();
        serializator.deserializeCarWithSetupDefult(pathToSerialization).GetFullInfoAboutThisCar();
        serializator.deserializeCarWithChangeColor(pathToSerialization, "green").GetFullInfoAboutThisCar();
    }
}
