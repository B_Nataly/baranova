import java.io.Serializable;
// класс, который можно сериализовать
public class SportCar implements Serializable {
    private String color = "non";
    private String vendor = "non";
    private String mark ="non";
    private int maxSpeed=1;
    private double acceleration=1;
    private boolean isAutomatic=false;
    public SportCar(String color, String vendor, String mark, int maxSpeed, double acceleration, boolean isAutomatic)
    {
        this.color = color;
        this.vendor = vendor;
        this.mark =mark;
        this.maxSpeed = maxSpeed;
        this.acceleration =acceleration;
        this.isAutomatic = isAutomatic;
    }
    private int GetTimeForGetMaxSpeed()
    {
        int time=1;
        double currentSpeed=0;
        double accelerationInKMperSeconds = acceleration/1000;
        while (currentSpeed<maxSpeed){
            currentSpeed+=accelerationInKMperSeconds*time;
            time++;
        }
        return time;
    }

    public void GetFullInfoAboutThisCar()
    {
        String typeOfSwitch;
        if(isAutomatic){typeOfSwitch="automatic";}
        else {typeOfSwitch = "manual";}
        System.out.println("This car '"+mark+"' from '"+vendor + "'. It is a " +color+", with "+typeOfSwitch+" speed change. The max speed is: "+ maxSpeed+
        ". For take this max speed this cur needs only '"+GetTimeForGetMaxSpeed() +" seconds");
    }

    //метод для юнит тестов для сравнения разных объектов
    public String GetFullInfoAboutThisCarAsString()
    {
        String typeOfSwitch;
        if(isAutomatic){typeOfSwitch="automatic";}
        else {typeOfSwitch = "manual";}
        return "This car '"+mark+"' from '"+vendor + "'. It is a " +color+", with "+typeOfSwitch+" speed change. The max speed is: "+ maxSpeed+
                ". For take this max speed this cur needs only '"+GetTimeForGetMaxSpeed() +" seconds";
    }

    //метод для рефлексии, чтобы менять поля класса
    public void SetupDefaultValues()
    {
        this.color = "non";
        this.vendor = "non";
        this.mark ="non";
        this.maxSpeed = 1;
        this.acceleration =1;
        this.isAutomatic = false;
    }
}
