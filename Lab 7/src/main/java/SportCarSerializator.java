import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class SportCarSerializator {

    private SportCar car;

    public SportCarSerializator(SportCar car)
    {
        this.car = car;
    }

    public String serializeCar(String filename) {
        System.out.println("Start serialization");
        try {
//создание цепи потоков с потоком вывода объекта в конце
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename));
            out.writeObject(car);
            out.close();
        } catch (IOException ex) {
            System.out.println("Cloud not write serialize objects to " + filename + ". More information :" + ex.getMessage());
        }
        return filename;
    }
    // метод, который обрабатывает эксепшен и вернет пустой объект
    public SportCar deserialize(String filename)
    {
        SportCar car = null;
        System.out.println("Start deserialization");
        try {
            //читаем данные из файла
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename));
            car = (SportCar) in.readObject();
            in.close();
        } catch (IOException ex) {
            System.out.println("Cloud not write deserialize objects to " + filename + ". More information :" + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println("Cloud not find such class in package. More information :" + ex.getMessage());
        }
        return car;
    }
    // метод, который обрабатывает эксепшен и вернет пустой объект
    public SportCar deserializeCarWithChangeColor(String filename, String colorToSetup)
    {
        SportCar desrializedCar = this.deserialize(filename);
        try {
            Field f = desrializedCar.getClass().getDeclaredField("color");
            f.setAccessible(true);
            f.set(desrializedCar, colorToSetup);
        } catch (IllegalAccessException ex) {
            System.out.println("Cloud not call private field or method. More information :" + ex.getMessage());
        } catch (NoSuchFieldException ex) {
            System.out.println("Cloud not find field. More information :" + ex.getMessage());
        }
        return desrializedCar;
    }
    // метод, который обрабатывает эксепшен и вернет пустой объект
    public SportCar deserializeCarWithSetupDefult(String filename) {
        Object carObject = null;
        System.out.println("Start deserialization");
        try {
            //читаем данные из файла
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename));
            carObject = in.readObject();
            in.close();
            //работаем с car посредством рефлексии
            Class car = carObject.getClass(); // использование рефлексии, чтобы поменять поля в классе
            car.getMethod("SetupDefaultValues").invoke(carObject, null);// вызываем метод SetupDefaultValues, чтобы сбросить все поля на дефолтное значение

        } catch (IOException ex) {
            System.out.println("Cloud not write deserialize objects to " + filename + ". More information :" + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            System.out.println("Cloud not find such class in package. More information :" + ex.getMessage());
        } catch (NoSuchMethodException ex) {
            System.out.println("Cloud not find method in the class. More information :" + ex.getMessage());
        } catch (InvocationTargetException ex) {
            System.out.println("Cloud not call method with this parameters More information :" + ex.getMessage());
        } catch (IllegalAccessException ex) {
            System.out.println("Cloud not call private field or method. More information :" + ex.getMessage());
        }
        return (SportCar) carObject;
    }
}
