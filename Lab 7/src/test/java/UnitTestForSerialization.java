import org.junit.*;
import java.io.File;

public class UnitTestForSerialization {

    SportCar testCar;
    SportCar testEmptyCar;
    SportCarSerializator testSerializator;
    String testPathForSavedSerializedCar ="cars.dat";
    String testColor = "Green";
    @Before
    public void setupDataBeforeAllTest()
    {
        this.testCar = new SportCar("blue", "BMW", "X7", 250, 3, false);
        this.testEmptyCar = new SportCar("blue", "BMW", "X7", 250, 3, false);
        this.testEmptyCar.SetupDefaultValues();
        this.testSerializator = new SportCarSerializator(testCar);
        File serializedFile = new File(testPathForSavedSerializedCar);
        if (serializedFile.exists())
        {
            serializedFile.delete();
        }
    }

    @Test
    public void testSerialization() throws Exception
    {
        Assert.assertEquals(testPathForSavedSerializedCar, testSerializator.serializeCar(testPathForSavedSerializedCar));
    }

    @Test
    public void testSerializationFileExist() throws Exception
    {
        File serializedFile = new File(testSerializator.serializeCar(testPathForSavedSerializedCar));
        Assert.assertTrue(serializedFile.exists());
    }

    @Test
    public void testSerializationFileNotEmpty() throws Exception
    {
        File serializedFile = new File(testSerializator.serializeCar(testPathForSavedSerializedCar));
        Assert.assertTrue(serializedFile.length()>0);
    }

    @Test
    public void testDeserialization() throws Exception
    {
        testSerializator.serializeCar(testPathForSavedSerializedCar);
        String testString = testCar.GetFullInfoAboutThisCarAsString();
        String outputString = testSerializator.deserialize(testPathForSavedSerializedCar).GetFullInfoAboutThisCarAsString();
        Assert.assertTrue(outputString.contains(testString));
    }

    @Test
    public void testDeserializationColor() throws Exception
    {
        testSerializator.serializeCar(testPathForSavedSerializedCar);
        String testString = testCar.GetFullInfoAboutThisCarAsString();
        String outputString = testSerializator.deserializeCarWithSetupDefult(testPathForSavedSerializedCar).GetFullInfoAboutThisCarAsString();
        Assert.assertFalse(outputString.equals(testString));
    }
    @Test
    public void testDeserializationColor_checkColor() throws Exception
    {
        testSerializator.serializeCar(testPathForSavedSerializedCar);
        String outputString = testSerializator.deserializeCarWithChangeColor(testPathForSavedSerializedCar, testColor).GetFullInfoAboutThisCarAsString();
        String testString ="'. It is a " +testColor+", with ";
        Assert.assertTrue(outputString.contains(testString));
    }

    @Test
    public void testDeserializationDefault() throws Exception
    {
        testSerializator.serializeCar(testPathForSavedSerializedCar);
        String testString = testCar.GetFullInfoAboutThisCarAsString();
        String outputString = testSerializator.deserializeCarWithSetupDefult(testPathForSavedSerializedCar).GetFullInfoAboutThisCarAsString();
        Assert.assertFalse(outputString.equals(testString));
    }

    @Test
    public void testDeserializationDefault_WithEmptyCar() throws Exception
    {
        testSerializator.serializeCar(testPathForSavedSerializedCar);
        String testString = testEmptyCar.GetFullInfoAboutThisCarAsString();
        String outputString = testSerializator.deserializeCarWithSetupDefult(testPathForSavedSerializedCar).GetFullInfoAboutThisCarAsString();
        Assert.assertTrue(outputString.equals(testString));
    }

    @Test
    public void testDeserializationWithoutSerialization() throws Exception
    {
        Assert.assertNull(testSerializator.deserialize(testPathForSavedSerializedCar));
    }

    @Test
    public void testDeserializationWithWrongPath() throws Exception
    {
        testSerializator.serializeCar(testPathForSavedSerializedCar);
        String wrongPath="testwrongPath";
        Assert.assertNull(testSerializator.deserialize(wrongPath));
    }
}
