import org.junit.*;
import java.io.IOException;
import java.util.List;
import static org.junit.Assert.*;

public class WeatherAPI {
    AutoAPILab logicClass = new AutoAPILab();
    String resourceFile = "src/main/resources/weatherdata.txt";

    @Test
    public void checkApiCallsByFile() throws IOException {
        List<String> inputData = logicClass.readFile(resourceFile);
        for (int i = 0; i < inputData.size(); i++) {
            String[] splitedData = inputData.get(i).split("   ");
            boolean isContains = logicClass.checkResponseContains(splitedData[0], splitedData[1]);
            if (isContains)
                assertTrue("called Api method '" + splitedData[0] + "'contains data: " + splitedData[1] + "'",
                        isContains);
            else assertFalse("called Api method '" + splitedData[0] + "'doesn't contain data: " + splitedData[1] + "'",
                    isContains);
        }
    }
}
