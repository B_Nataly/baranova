/**
 * Created by natalybaranova on 4/25/2016.
 */
import java.util.Collection;
import java.util.Iterator;
import java.lang.Object;
import java.util.List;
import java.util.ListIterator;

public class myArrayList<E> implements List<E> {

    private myNode<E> previous;
    private myNode<E> next;
    private myNode<E> first;

    private int size = 0;

    public myArrayList() {
        this.previous = new myNode<E>();
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < size(); i++) {
            if (get(i).equals(o)) return true;
        }
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[size];
        for (int i = 0; i < size; i++) {
            array[i] = getByIndex(i).getT();
        }
        return array;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(E e) {
        myNode<E> node = new myNode<E>(e);
        if (first == null) {
            first = node;
        } else {
            previous = this.getByIndex(this.size()-1);
            previous.setNext(node);
        }
        size++;
        return true;
    }

    private myNode<E> getByIndex(int index) {
        myNode<E> node = null;
        if (!isEmpty() && (index >= 0 && index < size)) {
            node = first;
            for (int i = 1; i <= index; i++) {
                node = node.getNext();
            }
        }
        return node;
    }
    @Override

    public boolean remove(Object o) {
        myNode<E> node = first;
        for (int i = 0; i < size(); i++) {
            if (node.equals(o)) {
                node.getPrevious().setNext(node.getNext());
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public E get(int index) {
        E element;
        if (index >= 0 && index < size()) {
            element = getByIndex(index).getT();
        } else throw new IndexOutOfBoundsException();
        return element;
    }

    @Override
    public E set(int index, E element) {
        return null;
    }

    @Override
    public void add(int index, E element) {

    }

    @Override
    public E remove(int index) {
        return null;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }
}

