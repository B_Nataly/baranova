/**
 * Created by natalybaranova on 4/25/2016.
 */
import java.util.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class RunList {
    public RunList() throws ParseException {
    }

    public static void main(String args[]) throws ParseException, MyException {
        List<String> dateList = new myArrayList<>();
        dateList.add("27.04.1955");
        dateList.add("19.05.1963");
        dateList.add("25.07.1988");
        dateList.add("02.10.1969");
        dateList.add("06.09.1980");
        dateList.add("22.04.1990");
        dateList.add("24.08.1997");
        dateList.add("29.06.1989");
        dateList.add("20.10.2011");
        dateList.add("11.02.2012");
        dateList.add("12.11.2014");
        dateList.add("28.09.1959");
        dateList.add("21.12.1989");
        dateList.add("23.08.2010");
        dateList.add("19.01.2013");
        dateList.add("01.04.1957");
        dateList.add("03.02.1968");
        dateList.add("07.07.1962");
        dateList.add("04.03.1971");
        dateList.add("26.05.1983");
        dateList.add("05.04.1972");
        dateList.add("09.06.1994");
        dateList.add("10.07.1995");
        dateList.add("13.10.2011");
        dateList.add("14.08.2010");
        dateList.add("15.09.1984");
        dateList.add("08.10.1988");
        dateList.add("16.11.1998");
        dateList.add("17.12.2007");
        dateList.add("18.12.2005");

        for (int i = 0; i < dateList.size(); i++) {
            SimpleDateFormat format = new SimpleDateFormat();
            format.applyPattern("dd.MM.yyyy");
            Date docDate = format.parse(dateList.get(i));
            checkWeekend(docDate);
        }
    }

    private static void checkWeekend(Date d) throws MyException, MyRuntimeException {
        Calendar startDate = Calendar.getInstance();
        startDate.setTime(d);
        try {
            if (startDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || startDate.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
                new MyException(d);
            } else {
                new MyRuntimeException(d);
            }
        } catch (RuntimeException e) {
            System.out.println(d + "- будний");
        } catch (Exception e) {
            System.out.println(d + "- выходной");
        }
    }
}