/**
 * Created by natalybaranova on 4/25/2016.
 */
public class myNode<T> {

    private myNode next;
    private myNode previous;
    private T t;

    public myNode() {
        this.next = null;
        this.previous = null;
    }

    public myNode(myNode previous) {
        this.next = null;
        this.previous = previous;
    }

    public myNode(T t) {
        this.t = t;
    }

    @Override
    public boolean equals(Object obj) {
        return t.equals(obj);
    }

    public myNode getNext() {
        return next;
    }

    public void setNext(myNode next) {
        this.next = next;
    }

    public myNode getPrevious() {
        return previous;
    }

    public void setPrevious(myNode previous) {
        this.previous = previous;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }
}
