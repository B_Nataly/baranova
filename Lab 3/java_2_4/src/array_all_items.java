import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;


public class array_all_items {
    public static void main(String[] args) {
        Random rand = new Random();
        // create random int values
        int[] myArray = new int[4];
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = rand.nextInt();
        }
        for (int i : myArray) {
            System.out.println(i);
        }
    }
}