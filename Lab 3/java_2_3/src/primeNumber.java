/**
 * Created by natalybaranova on 4/8/2016.
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class primeNumber {

    static int amount = 0;

    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите число от 1 до 10000: ");
        amount = Integer.parseInt(reader.readLine());
        System.out.println("Простые числа:");
        vivod_chisel();
    }

    public static void vivod_chisel() {
        int currentNumber, divideTimes;
        for (currentNumber = 2; currentNumber < amount; currentNumber++) {
            divideTimes = 0;
            for (int i = 1; i <= currentNumber; i++) {
                if (currentNumber % i == 0)
                    divideTimes++;
            }
            if (divideTimes == 2)
                System.out.println(currentNumber);
        }
    }
}
