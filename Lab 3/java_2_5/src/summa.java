/**
 * Created by natalybaranova on 4/8/2016.
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.NumberFormat;

public class summa {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        double sum = 0;
        while (true) {
            System.out.println("Введите целое или дробное число:");
            String num = reader.readLine();
            // check if "summa" is entered
            if (num.equals("summa")) {
                System.out.printf("Сумма всех введенных чисел = " + NumberFormat.getInstance().format(sum));
                return;
            }
            double number = Double.parseDouble(num);
            sum += number;
        }
    }
}
