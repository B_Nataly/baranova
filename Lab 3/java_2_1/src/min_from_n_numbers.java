/**
 * Created by natalybaranova on 4/8/2016.
 */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Random;


public class min_from_n_numbers {
    static int rangeForRandom = 100000;
    static int offsetForRandom = 15000;

    public static void main(String[] args) throws Exception {
        //entering the number of items
        System.out.println("Введите n (целое положительное число): ");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String n = reader.readLine();
        int amount = Integer.parseInt(n);
        //create random int values
        Random rand = new Random();
        int[] myArray = new int[amount];
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = rand.nextInt(rangeForRandom) - offsetForRandom;
        }
        for (int i : myArray) {
            System.out.println(i + " ");
        }
        System.out.println("Минимальное число в массиве: " + minimum(myArray));
    }

    //finding the minimum number
    public static int minimum(int[] someArray) {
        int min = someArray[0];
        for (int i = 1; i < someArray.length; i++) {
            if (min > someArray[i])
                min = someArray[i];
        }
        return min;
    }
}