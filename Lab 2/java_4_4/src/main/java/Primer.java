/**
 * Created by natalybaranova on 4/7/2016.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Primer {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Type the first number (integer or double): ");
        String num1 = reader.readLine();
        System.out.println("Type one from the following operations: +, -, *, /, %");
        String sign = reader.readLine();
        System.out.println("Type the second number (integer or double): ");
        String num2 = reader.readLine();

        double number1 = Double.parseDouble(num1);
        double number2 = Double.parseDouble(num2);
        char operation = sign.charAt(0);

        switch (operation) {
            case '*':
                System.out.println("Result: " + (number1 * number2));
                break;
            case '/':
                System.out.println("Result: " + (number1 / number2));
                break;
            case '-':
                System.out.println("Result: " + (number1 - number2));
                break;
            case '%':
                System.out.println("Result: " + (number1 % number2));
                break;
            case '+':
                System.out.println("Result: " + (number1 + number2));
                break;
            default: {
                System.out.println("You have typed incorrect mathematical operation.");
            }
        }
    }
}
