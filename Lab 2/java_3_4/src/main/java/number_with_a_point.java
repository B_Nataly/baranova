/**
 * Created by natalybaranova on 4/6/2016.
 */
public class number_with_a_point {

    public static void main(String[] args) {
        doubleNumber(545.678);
        intNumber(3245235);
    }

    public static void doubleNumber(double a) {
        int b = (int) a;
        System.out.println(b);
    }

    public static void intNumber(int a) {
        float c = (float) a;
        System.out.println(c);
    }
}
