/**
 * Created by natalybaranova on 4/5/2016.
 */

public class Operators {
    public static void main(String[] args) {
        System.out.println("a+b=" + sum(5, 6));
        System.out.println("a-b=" + minus(-5, -8));
        System.out.println("a*b=" + multiply(11, 11));
        System.out.println("a/b=" + division(12, 5));
        compare(345, 14);
        and_or(5, 3, 6);
        increment(5);
    }

    public static int sum(int a, int b) {
        return a + b;
    }

    public static int minus(int a, int b) {
        return a - b;
    }

    public static int multiply(int a, int b) {
        return a * b;
    }

    public static double division(int a, int b) {
        return a / b;
    }

    public static void compare(int a, int b) {
        if (a > b) {
            System.out.println("a>b");
        } else {
            if (a < b) System.out.println("a<b");
            else {
                System.out.println("a=b");
            }
        }
    }

    public static void and_or(int a, int b, int c) {
        if (a > c && b > c) {
            System.out.println("c is the smallest value.");
        } else if (a > c || b > c) {
            System.out.println("c is less than a or b.");
        } else {
            System.out.println("c isn't less than a or b.");
        }
    }

    public static void increment(int k) {
        System.out.println(k += 10);
    }
}