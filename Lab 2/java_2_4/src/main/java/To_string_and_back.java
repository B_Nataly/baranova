/**
 * Created by natalybaranova on 4/6/2016.
 */
public class To_string_and_back {
    public static void main(String[] args) {

        int myInt = 5;
        float myFloat = 4.6f;
        double myDouble = 4.14;
        long myLong = 4678954;
        char myChar = 'M';
        boolean myBoolean = false;
        byte myByte = 10;
        short myShort = 3;

        String myString1 = "3";
        String myString2 = "5.6";
        String myString3 = "4.1";
        String myString4 = "4353456546";
        String myString5 = "array";
        String myString6 = "true";
        String myString7 = "34";
        String myString8 = "6";

        int_to_string(myInt);
        float_to_string(myFloat);
        double_to_string(myDouble);
        long_to_string(myLong);
        char_to_string(myChar);
        boolean_to_string(myBoolean);
        byte_to_string(myByte);
        short_to_string(myShort);

        string_to_int(myString1);
        string_to_float(myString2);
        string_to_double(myString3);
        string_to_long(myString4);
        string_to_char(myString5);
        string_to_boolean(myString6);
        string_to_byte(myString7);
        string_to_short(myString8);
    }

    //int to string
    public static void int_to_string(int a) {
        String b = Integer.toString(a);
        System.out.println("Text" + b);
    }

    //float to string
    public static void float_to_string(float a) {
        String c = Float.toString(a);
        System.out.println("Text" + c);
    }

    //double to string
    public static void double_to_string(double a) {
        String d = Double.toString(a);
        System.out.println("Text" + d);
    }

    //long to string
    public static void long_to_string(long a) {
        String e = Long.toString(a);
        System.out.println("Text" + a);
    }

    //char to string
    public static void char_to_string(char a) {
        String f = Character.toString(a);
        System.out.println("Text" + f);
    }

    //boolean to string
    public static void boolean_to_string(boolean a) {
        String g = Boolean.toString(a);
        System.out.println("Text" + g);
    }

    //byte to string
    public static void byte_to_string(byte a) {
        String h = Byte.toString(a);
        System.out.println("Text" + h);
    }

    //short to string
    public static void short_to_string(short a) {
        String j = Short.toString(a);
        System.out.println("Text" + j);
    }

    //string to int
    public static void string_to_int(String a) {
        Integer k = Integer.valueOf(a);
        System.out.println(6 + k);
    }

    //string to float
    public static void string_to_float(String a) {
        Float kk = Float.valueOf(a);
        System.out.println(5.4 + kk);
    }

    //string to float
    public static void string_to_double(String a) {
        Double m = Double.valueOf(a);
        System.out.println(1.2 + m);
    }

    //string to long
    public static void string_to_long(String a) {
        Long n = Long.valueOf(a);
        System.out.println(4564 + n);
    }

    //string to char
    public static void string_to_char(String a) {
        char[] nn = a.toCharArray();
        System.out.println(nn);
    }

    //string to boolean
    public static void string_to_boolean(String a) {
        Boolean p = Boolean.parseBoolean(a);
        System.out.println(p);
    }

    //string to byte
    public static void string_to_byte(String a) {
        Byte z = Byte.valueOf(a);
        System.out.println(2 + z);
    }

    //string to short
    public static void string_to_short(String a) {
        Short x = Short.valueOf(a);
        System.out.println(1 + x);
    }
}