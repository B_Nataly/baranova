/**
 * Created by Натали on 27.05.2016.
 */
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;

public class TestStackoverflow {
    private static WebDriver driver;
    private static String baseUrl = "http://stackoverflow.com";

    @AfterClass
    public static void tearDown() throws Exception{
        driver.quit();
    }

    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.get(baseUrl);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Before
    public void setUpBefore() throws Exception {
        if(driver == null){
            setUp();}
        else{
            if (!driver.getCurrentUrl().equals(baseUrl+"/")) {
                driver.get(baseUrl);
            }
        }
    }
    @Test
    public void checkCount() throws InterruptedException {
        WebElement value = driver.findElement(By.xpath("//*[@id='tabs']//span[contains(@class, 'bounty-indicator-tab')]"));
        int count = Integer.parseInt( value.getText());
        Assert.assertTrue("The featured count not greater than 300!", count >= 300);
    }

    @Test
    public void checkLoginButtons() throws InterruptedException{
        driver.findElement(By.xpath("//a[contains(text(), 'sign up')]")).click();
        Assert.assertTrue("Google login button is absent!",
                driver.findElement(By.xpath(".//*[@id='openid-buttons']//div[contains(@class, 'google-login')]")).isDisplayed());//проверка ключевого элемента
        Assert.assertTrue("Facebook login button is absent!",
                driver.findElement(By.xpath("//*[@id='openid-buttons']//div[contains(@class, 'facebook-login')]")).isDisplayed());//проверка ключевого элемента
    }

    @Test
    public void checkQuestionDate() throws InterruptedException{
        driver.findElement(By.xpath("//a[@class='question-hyperlink']")).click(); // клик по элементу
        String day = driver.findElement(By.xpath("//*[@id='qinfo']//p/b[contains(., 'today')]")).getText();
        Assert.assertTrue("Question was asked not today!", day.equals("today"));
    }
}