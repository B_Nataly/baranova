/**
 * Created by Натали on 27.05.2016.
 */
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;

public class TestRozetka {
    private static WebDriver driver;
    private static String baseUrl = "http://rozetka.com.ua";

    @AfterClass

    public static void tearDown() throws Exception{
        driver.quit();
    }

    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.get(baseUrl);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Before
    public void setUpBefore() throws Exception {
        if(driver == null){
            setUp();}
        else{
            if (!driver.getCurrentUrl().equals(baseUrl+"/")) {
                driver.get(baseUrl);
            }
        }
    }

    @Test
    public void checkLogo() throws InterruptedException{
        Assert.assertTrue("'Rozetka' logo isn't displayed!",
                driver.findElement(By.xpath("//*[@id='body-header']")).isDisplayed());//проверка ключевого элемента
    }

    @Test
    public void checkAppleItem() throws InterruptedException {
        Assert.assertTrue("The catalog doesn't contain 'Apple' item!",
                driver.findElement(By.xpath("//*[@id='m-main']/li/a[contains(.,'Apple')]")).isDisplayed());//проверка ключевого элемента
    }

    @Test
    public void checkMP3text() throws InterruptedException {
        Assert.assertTrue("The catalog doesn't contain item with 'MP3' text!",
                driver.findElement(By.xpath("//*[@id='m-main']/li/a[contains(.,'MP3')]")).isDisplayed());//проверка ключевого элемента
    }

    @Test
    public void checkCities() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id='city-chooser']/a")).click(); //проверка ключевого элемента
        Assert.assertTrue("The city is absent!",
                driver.findElement(By.xpath("//*[@id='city-chooser']//a[contains(.,'Киев')]")).isDisplayed()); //проверка ключевого элемента
        Assert.assertTrue("The city is absent!",
                driver.findElement(By.xpath("//*[@id='city-chooser']//a[contains(.,'Харьков')]")).isDisplayed());//проверка ключевого элемента
        Assert.assertTrue("The city is absent!",
                driver.findElement(By.xpath("//*[@id='city-chooser']//a[contains(.,'Одесса')]")).isDisplayed());//проверка ключевого элемента
    }

    @Test
    public void checkCart() throws InterruptedException {
        driver.findElement(By.xpath("//*[@id='cart_popup_header']//a[contains(@onclick, 'openCart')]")).click(); //клик по элементу
        Assert.assertTrue("The basket is not empty!",
                driver.findElement(By.xpath("//*[@id='drop-block']/h2")).isDisplayed()); //проверка ключевого элемента
    }
}