package StackoverflowPageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class StackoverflowQuestions {
    private WebDriver driver;

    public StackoverflowQuestions(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;

    }

    public boolean CheckQuestion() throws Exception {
        return driver.findElement(By.xpath("//*[@id='qinfo']//p/b[contains(., 'today')]")).getText().equals("today");
    }
}
