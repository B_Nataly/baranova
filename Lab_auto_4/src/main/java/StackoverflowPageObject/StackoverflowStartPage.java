package StackoverflowPageObject;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StackoverflowStartPage {
    private WebDriver driver;

    public StackoverflowStartPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = "//*[@id='tabs']//span[contains(@class, 'bounty-indicator-tab')]")
    public WebElement count;

    public boolean CheckCount(int expectedCount) throws Exception {
        String textCount = count.getText();
        int featuredCount = Integer.parseInt(textCount);
        return featuredCount > expectedCount;

    }

    @FindBy(xpath = "//a[contains(text(), 'sign up')]")
    public WebElement sign;

    public StackoverflowSignUp ClickSign() {
        sign.click();
        return new StackoverflowSignUp(driver);
    }

    @FindBy(xpath = "//a[@class='question-hyperlink']")
    public WebElement question;

    public StackoverflowQuestions ClickQuestion() {
        question.click();
        return new StackoverflowQuestions(driver);
    }
}
