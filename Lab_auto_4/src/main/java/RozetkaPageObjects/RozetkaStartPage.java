package RozetkaPageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RozetkaStartPage {
    private WebDriver driver;

    public RozetkaStartPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;

    }

    @FindBy(xpath = "//*[@id='body-header']")
    public WebElement logo;

    @FindBy(xpath = "//*[@id='m-main']/li/a[contains(.,'Apple')]")
    public WebElement apple;

    @FindBy(xpath = "//*[@id='m-main']/li/a[contains(.,'MP3')]")
    public WebElement mp3;

    @FindBy(xpath = "//*[@id='city-chooser']/a")
    public WebElement cityChooser;

    public RozetkaCities ClickLink() {
        cityChooser.click();
        return new RozetkaCities(driver);
    }

    @FindBy(xpath = "//*[@id='cart_popup_header']//a[contains(@onclick, 'openCart')]")
    public WebElement cart;

    public Cart ClickCart() {
        cart.click();
        return new Cart(driver);
    }

    public boolean GetLinkFromCatalog(String linkName){
        switch (linkName)
        {
            case("MP3"): return mp3.isDisplayed();
            case("Apple"):return apple.isDisplayed();
            default: return false;
        }
    }
}