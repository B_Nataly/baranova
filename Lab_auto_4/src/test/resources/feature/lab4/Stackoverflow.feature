@Stackoverflow @feature
Feature: Stackoverflow Feature

  Background:
    Given I navigate to start page of Stackoverflow website

  @_001_ @high
  Scenario: As user I want to check count on start page
    Then I want to see that the featured count greater than 300

  @_002_@high
  Scenario: As user I want check see buttons google and facebook for authorization
    When I click 'SignUP' link
    Then I want to see button "Google" in sign up page
    And I want to see button "Facebook" in sign up page

  @_003_
  Scenario: As user I want to see question that was asked today
    When I click to question button for navigation to Question page
    Then I want to see that it is at least one question on the question page