@Rozetka @feature
Feature: Rozetka Feature

  Background:
    Given I navigate to start page of "Rozetka" website

  @_001_ @high
  Scenario: Displayed logo
    Then I want to see this logo is displayed

  @_002_@high
  Scenario: Display MP3 in catalog
    Then I want to see this "MP3" in catalog

  @_003_
  Scenario: Display Apple production in catalog
    Then I want to see this "Apple" in catalog

  @_004_
  Scenario: Display some sities in choose sity pop-up
    When I click to "Choose city"
    Then I want to see 'Харьков', 'Киев' and 'Одесса' city names

  @_005_
  Scenario: As user I can open empty basket
    Given I navigate to start page of "Rozetka" website
    When I click to "Корзина" link
    Then I want to see basket popup