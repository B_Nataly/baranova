package feature.lab4;

import cucumber.api.CucumberOptions;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

@RunWith(Cucumber.class)

@CucumberOptions(monochrome = true,
        tags = {"@_004_"},
        features = "src/test/resources/feature/",
        format = {"pretty", "html: cucumber-html-reports",
                "json: cucumber-html-reports/cucumber.json"}
)
public class FeatureRunner {
    private static String urlForTest;
    public static WebDriver driver;

    @BeforeClass
    public static void setupFeatureRunner() throws Exception {
        driver = new FirefoxDriver();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        driver.quit();
    }
}