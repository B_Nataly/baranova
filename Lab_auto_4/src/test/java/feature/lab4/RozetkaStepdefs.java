package feature.lab4;

import RozetkaPageObjects.*;
import cucumber.api.Format;
import cucumber.api.PendingException;
import cucumber.api.java.en.*;
import org.junit.AfterClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.Assert.*;

import cucumber.api.java.*;

import java.util.concurrent.TimeUnit;

public class RozetkaStepdefs {

    private static WebDriver driver;
    private RozetkaStartPage rozetkaStartPage;
    private RozetkaCities rozetkaCities;
    private Cart cart;
    private static String baseUrl = "http://rozetka.com.ua";
    FeatureRunner runner = new FeatureRunner();


    @Given("^I navigate to start page of \"(.*?)\" website$")
    public void i_navigate_to_start_page_of_website(String arg1) throws Throwable {
        driver = runner.driver;
        driver.get(baseUrl);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        rozetkaStartPage = new RozetkaStartPage(driver);
    }

    @Then("^I want to see this logo is displayed$")
    public void i_want_to_see_that_logo_is_displayed() throws Throwable {
        assertTrue("'Rozetka' logo isn't displayed!", this.rozetkaStartPage.logo.isDisplayed());
    }

    @Then("^I want to see this \"([^\"]*)\" in catalog$")
    public void i_want_to_see_that_in_catalog(String linkText) throws Throwable {
        assertTrue("The catalog contains '" + linkText + "' item!", this.rozetkaStartPage.GetLinkFromCatalog(linkText));
    }

    @When("^I click to \"(.*?)\"$")
    public void i_click_to(String arg1) throws Throwable {
        this.rozetkaCities = this.rozetkaStartPage.ClickLink();
    }

    @Then("^I want to see 'Харьков', 'Киев' and 'Одесса' city names$")
    public void i_want_to_see_Харьков_Киев_and_Одесса_city_names() throws Throwable {
        assertTrue("One city from the list is absent!", this.rozetkaCities.isCitiesDisplayed());
    }

    @When("^I click to \"(.*?)\" link$")
    public void i_click_to_link(String arg1) throws Throwable {
        this.cart = this.rozetkaStartPage.ClickCart();
    }

    @Then("^I want to see basket popup$")
    public void i_want_to_see_basket_popup() throws Throwable {
        assertTrue("The basket is not empty!", this.cart.emptyCart.isDisplayed());
    }
}
