package feature.lab4;

import StackoverflowPageObject.StackoverflowQuestions;
import StackoverflowPageObject.StackoverflowSignUp;
import StackoverflowPageObject.StackoverflowStartPage;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class StackoverflowStepdefs {

    private static WebDriver driver;
    private StackoverflowQuestions stackoverflowQuestions;
    private StackoverflowSignUp stackoverflowSignUp;
    private StackoverflowStartPage stackoverflowStartPage;
    private static String baseUrl = "http://stackoverflow.com";
    FeatureRunner runner = new FeatureRunner();


    @Given("^I navigate to start page of Stackoverflow website$")
    public void iNavigateToStartPageOfStackoverflowWebsite() throws Throwable {
        driver = runner.driver;
        driver.get(baseUrl);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        stackoverflowStartPage = new StackoverflowStartPage(driver);
    }

    @Then("^I want to see that the featured count greater than (\\d+)$")
    public void iWantToSeeThatTheFeaturedCountNotGreaterThan(int expectedCount) throws Throwable {
        Assert.assertTrue("The featured count not greater than 300!", stackoverflowStartPage.CheckCount(expectedCount));
    }

    @When("^I click 'SignUP' link$")
    public void iClickToSignUPLink() throws Throwable {
        stackoverflowSignUp = stackoverflowStartPage.ClickSign();
    }

    @Then("^I want to see button \"([^\"]*)\" in sign up page$")
    public void iWantSeeButtonInSignUpPage(String buttonName) throws Throwable {
        assertTrue("Google login button is absent!", stackoverflowSignUp.checkIsButtonDisplayed(buttonName));
    }


    @When("^I click to question button for navigation to Question page$")
    public void iClickToQuestionButtonForNavigateToQuestionPage() throws Throwable {
        stackoverflowQuestions = stackoverflowStartPage.ClickQuestion();
    }

    @Then("^I want to see that it is at least one question on the question page$")
    public void iWantSeeThatItWasAtLeatOneQuestionOnQuestionPage() throws Throwable {
        assertTrue("Question was asked not today!", stackoverflowQuestions.CheckQuestion());
    }
}