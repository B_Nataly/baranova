
import org.junit.*;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertTrue;

/**
 * Created by natalybaranova on 6/3/2016.
 */
public class Rozetka {
    private static WebDriver driver;
    private RozetkaStartPage rozetkaStartPage;
    private RozetkaCities rozetkaCities;
    private Cart cart;
    private static String baseUrl = "http://rozetka.com.ua";

    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.get(baseUrl);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Before
    public void setUpBefore() throws Exception {
        if (driver == null) {
            setUp();
        } else {
            if (!driver.getCurrentUrl().equals(baseUrl + "/")) {
                driver.get(baseUrl);
            }
        }
        if (rozetkaStartPage == null) {
            rozetkaStartPage = new RozetkaStartPage(driver);
        }
    }

    @AfterClass
    public static void tearDown() throws Exception {
        driver.quit();
    }

    @Test
    public void checkLogo() throws Exception {
        assertTrue("'Rozetka' logo isn't displayed!", rozetkaStartPage.logo.isDisplayed());
    }

    @Test
    public void checkAppleItem() throws Exception {
        assertTrue("The catalog doesn't contain 'Apple' item!", rozetkaStartPage.apple.isDisplayed());
    }

    @Test
    public void checkMP3text() throws Exception {
        assertTrue("Catalog doesn't contain item with 'MP3' text!", rozetkaStartPage.mp3.isDisplayed());
    }

    @Test
    public void checkCity() throws Exception {
        rozetkaCities = rozetkaStartPage.ClickLink();
        assertTrue("One city from the list is absent!", rozetkaCities.kiev.isDisplayed());
        assertTrue("One city from the list is absent!", rozetkaCities.kharkov.isDisplayed());
        assertTrue("One city from the list is absent!", rozetkaCities.odessa.isDisplayed());
    }

    @Test
    public void checkMyCart() throws Exception {
        cart = rozetkaStartPage.ClickCart();
        assertTrue("The basket is not empty!", cart.emptyCart.isDisplayed());
    }
}