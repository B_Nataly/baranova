import org.junit.*;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static junit.framework.TestCase.assertTrue;

public class Stackoverflow {
    private static WebDriver driver;
    StackoverflowStartPage stackoverflowStartPage;
    StackoverflowSignUp stackoverflowSignUp;
    StackoverflowQuestions stackoverflowQuestions;
    private static String baseUrl = "http://stackoverflow.com";

    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.get(baseUrl);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Before
    public void setUpBefore() throws Exception {
        if (driver == null) {
            setUp();
        } else {
            if (!driver.getCurrentUrl().equals(baseUrl + "/")) {
                driver.get(baseUrl);
            }
        }
        if (stackoverflowStartPage == null) {
            stackoverflowStartPage = new StackoverflowStartPage(driver);
        }
    }

    @AfterClass
    public static void tearDown() throws Exception {
        driver.quit();
    }

    @Test
    public void testCountFeatured() throws Exception {
        stackoverflowStartPage.CheckCount();
    }

    @Test
    public void checkLogin() throws Exception {
        stackoverflowSignUp = stackoverflowStartPage.ClickSign();
        assertTrue("Google login button is absent!", StackoverflowSignUp.googleLogin.isDisplayed());
        assertTrue("Facebook login button is absent!", StackoverflowSignUp.facebookLogin.isDisplayed());
    }

    @Test
    public void testQuestionAskedToday() throws Exception {
        stackoverflowQuestions = stackoverflowStartPage.ClickQuestion();
        stackoverflowQuestions.CheckQuestion();
    }
}