import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class StackoverflowSignUp {
    private WebDriver driver;

    public StackoverflowSignUp(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;

    }

    @FindBy(xpath = "//*[@id='openid-buttons']//div[contains(@class, 'google-login')]")
    public static WebElement googleLogin;

    @FindBy(xpath = "//*[@id='openid-buttons']//div[contains(@class, 'facebook-login')]")
    public static WebElement facebookLogin;
}

