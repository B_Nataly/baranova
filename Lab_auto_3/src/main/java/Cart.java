import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by natalybaranova on 6/3/2016.
 */
public class Cart {
    private WebDriver driver;

    public Cart(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;

    }

    @FindBy(xpath = "//*[@id='drop-block']/h2")
    public WebElement emptyCart;
}
