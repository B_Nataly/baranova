import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RozetkaCities {
    private WebDriver driver;

    public RozetkaCities(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;

    }

    @FindBy(xpath = "//*[@id='city-chooser']//div[@class='header-city-i']/*[contains(.,'Киев')]")
    public WebElement kiev;

    @FindBy(xpath = "//*[@id='city-chooser']//div[@class='header-city-i']/*[contains(.,'Харьков')]")
    public WebElement kharkov;

    @FindBy(xpath = "//*[@id='city-chooser']//div[@class='header-city-i']/*[contains(.,'Одесса')]")
    public WebElement odessa;
}
