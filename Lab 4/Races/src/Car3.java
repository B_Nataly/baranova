/**
 * Created by natalybaranova on 4/12/2016.
 */
public class Car3 extends Cars {
    // переменная для проверки была ли достигнута максимальная скорость
    private boolean maxSpeedIncreased = true;

    Car3(double maxSpeed, double acceleration, double maneuverability, String racerName) {
        super(maxSpeed, acceleration, maneuverability, racerName);
    }

    // если автомобиль в момент поворота достиг своей максимальной скорости, то его максимальная скорость увеличивается на 10% до конца гонки
    protected void turnRecalculation() {
        if (speed >= maxSpeed && maxSpeedIncreased) {
            maxSpeed += maxSpeed * 0.1;
            maxSpeedIncreased = false;
        }
        this.speed = speed * maneuverability;
    }
}
