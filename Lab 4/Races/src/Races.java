/**
 * Created by natalybaranova on 4/12/2016.
 */
public class Races {
    static int tracksCount = 20;

    // перевод в часы, минуты, секунды
    private static String getTime(int getTime) {
        int hours = getTime / 3600;
        int minutes = (getTime % 3600) / 60;
        int seconds = getTime % 60;
        return hours + " ч. " + minutes + " мин. " + seconds + " сек. ";
    }

    public static void main(String[] args) throws Exception {
        Cars[] x = new Cars[]{new Car1(180, 1.9, 0.2, "Sasha"), new Car2(180, 2.3, 0.3, "Oleg"), new Car3(160, 1.8, 0.3, "Nataly"), new Car3(100, 2.25, 1.3, "Ivan"), new Car1(155, 1.2, 0.5, "Masha")};
        for (int i = 0; i < tracksCount; i++) {
            for (int j = 0; j < x.length; j++) {
                x[j].driveAndTurn();
            }
        }
// сортировка результата по времени, затраченному на трассу
        for (int i = 0; i < x.length - 1; i++) {
            for (int j = 0; j < x.length - i - 1; j++) {
                if (x[j].getTime() > x[j + 1].getTime()) {
                    Cars t = x[j];
                    x[j] = x[j + 1];
                    x[j + 1] = t;
                }
            }
        }
        for (int i = 0; i < x.length; i++) {
            System.out.println(x[i].racerName + " прошел трассу за " + getTime(x[i].getTime()));
        }
    }
}