/**
 * Created by natalybaranova on 4/12/2016.
 */

public abstract class Cars {
    protected double speed = 0;

    public int getTime() {
        return time;
    }

    private int time = 0;
    protected double maxSpeed;
    protected double acceleration;
    protected double maneuverability;
    protected String racerName;

    Cars(double maxSpeed, double acceleration, double maneuverability, String racerName) {
        this.maxSpeed = maxSpeed * 1000 / 3600;
        this.acceleration = acceleration;
        this.maneuverability = maneuverability;
        this.racerName = racerName;
    }

    // расчет для отрезка пути
    public void Drive() {
        double trackedDistance = 0;
        int trackedTime = 0;
        while (trackedDistance < 2000) {
            //равномерное движение
            if (speed >= maxSpeed) {
                speed = maxSpeed;
                trackedTime++;
                trackedDistance += speed;
            }
            // равноускоренное движение
            else {
                speed += acceleration * trackedTime;
                trackedTime++;
                trackedDistance += speed; // distance/1 sec (time)
            }
        }
        //System.out.println("Гонщик " + racerName + " прошёл отрезок за " + trackedTime); // вывод результата после одного отрезка
        this.time += trackedTime;
    }

    // поворот, индивидуальный для каждой машины
    abstract void turnRecalculation();

    // отрезок + поворот
    protected void driveAndTurn() {
        Drive();
        turnRecalculation();
    }
}
