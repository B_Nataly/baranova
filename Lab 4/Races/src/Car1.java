/**
 * Created by natalybaranova on 4/12/2016.
 */
public class Car1 extends Cars {
    Car1(double maxSpeed, double acceleration, double maneuverability, String racerName) {
        super(maxSpeed, acceleration, maneuverability, racerName);
    }

    // если скорость на момент поворота больше половины от максимальной - автомобиль получает +0,5% от разницы между половиной от максимальной и нынешней к маневренности на этот поворот
    protected void turnRecalculation() {
        if (speed > maxSpeed / 2) {
            maneuverability = maneuverability + ((speed - maxSpeed / 2) / 20);
            this.speed = speed * maneuverability;
        }
    }
}
