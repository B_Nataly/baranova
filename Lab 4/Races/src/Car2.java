/**
 * Created by natalybaranova on 4/12/2016.
 */
public class Car2 extends Cars {
    Car2(double maxSpeed, double acceleration, double maneuverability, String racerName) {
        super(maxSpeed, acceleration, maneuverability, racerName);
    }

    // если скорость после поворота меньше половины от максимальной, то ускорение автомобиля на этом отрезке увеличивается в 2 раза
    protected void turnRecalculation() {
        this.speed = speed * maneuverability;
        if (speed < maxSpeed / 2) {
            acceleration = 2 * acceleration;
        } else {
            acceleration = 2;
        }
    }
}



